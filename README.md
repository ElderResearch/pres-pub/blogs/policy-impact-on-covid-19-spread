# Policy Impact on COVID-19 Spread

T. Shafer  
Updated August 24, 2020

This repository contains data and source code from our blog post "Policy Impact on COVID-19 Spread." To work with the data,

 1. Open `covid19_policy.Rproj` in RStudio.
 2. Initialize all necessary packages using `renv::activate()` and `renv::restore()`.
 3. Open `covid19_policy.rmd`.

Knitting the R Markdown document should generate intermediate data, models, and figures, though this might take several hours (or you might step through cell by cell).
